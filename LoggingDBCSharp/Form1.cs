﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Logging
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        log4net.ILog log;

        private void Form1_Load(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Button Info Klik");
            log.Info("Data berhasil disimpan ke database!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int x = 0;
            try
            {
                int y = 1 / x;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error catch in Exception");
                log.Error(ex.Message, ex);
            }
        }
    }
}
